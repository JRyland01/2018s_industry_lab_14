package ictgradschool.industry.swingworker.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * 
 * The application allows the user to enter a value for N, and then calculates 
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 *
 */
public class IntermediateResultsPrimeFactorsSwingApp extends JPanel {

	private JButton _startBtn;        // Button to start the calculation process.
	private JTextArea _factorValues;  // Component to display the result.
	private long n;
	private JButton _abortBtn;
	private PrimeFactorisationWorker p;

	public IntermediateResultsPrimeFactorsSwingApp() {

		// Create the GUI components.
		JLabel lblN = new JLabel("Value N:");
		final JTextField tfN = new JTextField(20);

		_startBtn = new JButton("Compute");
		_abortBtn = new JButton("Abort");
		_factorValues = new JTextArea();
		_factorValues.setEditable(false);




		// Add an ActionListener to the start button. When clicked, the
		// button's handler extracts the value for N entered by the user from
		// the textfield and find N's prime factors.
		_startBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				String strN = tfN.getText().trim();
				n = 0;

				try {
					n = Long.parseLong(strN);
				} catch(NumberFormatException e) {
					System.out.println(e);
				}

				// Disable the Start button until the result of the calculation is known.
				_startBtn.setEnabled(false);
				_abortBtn.setEnabled(true);

				// Clear any text (prime factors) from the results area.
				_factorValues.setText(null);

				// Set the cursor to busy.
				setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

				p =  new PrimeFactorisationWorker();
//				p.addPropertyChangeListener(this);
				p.execute();

				// Start the computation in the Event Dispatch thread.
//				 for (long i = 2; i*i <= n; i++) {
//
//			            // If i is a factor of N, repeatedly divide it out
//			            while (n % i == 0) {
//			                _factorValues.append(i + "\n");
//			                n = n / i;
//			            }
//			     }
//
//			     // if biggest factor occurs only once, n > 1
//			     if (n > 1) {
//			    	 _factorValues.append(n + "\n");
//			     }

//			     // Re-enable the Start button.
//				_startBtn.setEnabled(true);
//
//				// Restore the cursor.
//				setCursor(Cursor.getDefaultCursor());
//			}
		}});
		//action listener for abort button
		_abortBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				p.cancel(false);
			}
		});

		// Construct the GUI.
		JPanel controlPanel = new JPanel();
		controlPanel.add(lblN);
		controlPanel.add(tfN);
		controlPanel.add(_startBtn);
		controlPanel.add(_abortBtn);

		JScrollPane scrollPaneForOutput = new JScrollPane();
		scrollPaneForOutput.setViewportView(_factorValues);

		setLayout(new BorderLayout());
		add(controlPanel, BorderLayout.NORTH);
		add(scrollPaneForOutput, BorderLayout.CENTER);
		setPreferredSize(new Dimension(500,300));
	}
	public class PrimeFactorisationWorker extends SwingWorker <ArrayList<Long>,Long>{



		public long getN() {
			return n;
		}


		@Override
		protected ArrayList <Long> doInBackground() {
			ArrayList<Long> primeList = new ArrayList<>();
				for (long i = 2; i * i <= n; i++) {
					if(isCancelled()){
						return null;
					}
					// If i is a factor of N, repeatedly divide it out
					while (n % i == 0) {
						primeList.add(i);
						publish(i);
//						_factorValues.append(i + "\n");
						n = n / i;
						if(isCancelled()) {
							return null;
						}

					}
				}

				// if biggest factor occurs only once, n > 1
				if (n > 1) {
					primeList.add(n);
					publish(n);
//					_factorValues.append(n + "\n");
				}

				return primeList;

		}
		@Override
		protected void process(List<Long> pair){
			for (Long p: pair
				 ) {
				_factorValues.append(p + "\n");
			}

		}
		@Override
		protected void done(){
//			try {
//				ArrayList<Long> completedList = get();
//				for (long l: completedList) {
//					_factorValues.append(l +"\n");
//				}
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			} catch (ExecutionException e) {
//				e.printStackTrace();
//			}catch (CancellationException e){
//				_factorValues.append("Operation cancelled.");
//			}

			// Re-enable the Start button.
			_startBtn.setEnabled(true);
			_abortBtn.setEnabled(false);

			// Restore the cursor.
			setCursor(Cursor.getDefaultCursor());
		}
	}

	private static void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("Prime Factorisation of N");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		JComponent newContentPane = new IntermediateResultsPrimeFactorsSwingApp();
		frame.add(newContentPane);

		// Display the window.
		frame.pack();
        frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

